---
author: jmdemouy
title: 🏡 Cours
---

!!! danger Attention
     Bienvenus sur le site de PC des 1ères STI2D du lycée d'Altitude Briançon


!!! info "Adapter ce site modèle"

    Le tutoriel est ici : [Tutoriel de site avec python](https://docs.forge.apps.education.fr/tutoriels/pyodide-mkdocs-theme-review/){:target="_blank" }
    
    Si vous voulez conserver certaines pages de ce modèles sans qu'elles ne soient visibles dans le menu, il suffit de les enlever du fichier .pages   
    Vous les retrouverez facilement en utilisant la barre de recherche en haut à droite
    

[Exercices Python en ligne](https://codex.forge.apps.education.fr/){:target="_blank" }




<video controls src="https://upload.wikimedia.org/wikipedia/commons/d/d2/See_a_Koala_scratch%2C_yawn_and_sleep.webm"></video>